package com.xromatron.todo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DoneRq {
    boolean done;
}

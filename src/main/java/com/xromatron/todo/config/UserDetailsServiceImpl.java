package com.xromatron.todo.config;

import com.xromatron.todo.model.Account;
import com.xromatron.todo.model.Role;
import com.xromatron.todo.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final AccountService accountService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.debug("Trying to get account of {}", username);
        Account account = accountService.getByLogin(username)
                .orElseThrow(() -> {
                    String message = String.format("User '%s' not found", username);
                    log.info(message);
                    return new UsernameNotFoundException(message);
                });
        log.debug("Got account: {}", account);
        UserDetails userDetails = User.builder()
                .username(account.getLogin())
                .password(account.getPassword())
                .roles(account.getRoles().stream().map(Role::getRole).collect(Collectors.joining(",")))
                .build();
        log.debug("UserDetails: {}", userDetails);
        return userDetails;
    }

}

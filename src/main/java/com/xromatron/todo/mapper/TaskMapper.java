package com.xromatron.todo.mapper;

import com.xromatron.todo.dto.TaskDto;
import com.xromatron.todo.model.Task;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel="spring")
public interface TaskMapper {

    TaskMapper INSTANCE = Mappers.getMapper(TaskMapper.class);

    TaskDto toDto(Task task);

    List<TaskDto> toDto(List<Task> tasks);
}

package com.xromatron.todo.controller;

import com.xromatron.todo.model.Account;
import com.xromatron.todo.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping(value = "/account", headers = "Content-Type=application/json;charset=UTF-8")
@RestController
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping("/register")
    public String register(@RequestParam("login") String login, @RequestParam("password") String password) {
        Account account = accountService.saveUser(login, password);
        return String.format("Account of user '%s' created! Encoded password: '%s'", login, account.getPassword());
    }

}

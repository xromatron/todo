package com.xromatron.todo.controller;

import com.xromatron.todo.dto.DoneRq;
import com.xromatron.todo.dto.TaskDto;
import com.xromatron.todo.dto.TextRq;
import com.xromatron.todo.mapper.TaskMapper;
import com.xromatron.todo.model.Task;
import com.xromatron.todo.service.TaskService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

import static com.xromatron.todo.exception.TaskExceptionHandler.ErrorResponse;

@Slf4j
@Validated
@RestController
@RequestMapping("/tasks")
@RequiredArgsConstructor
@Tag(name = "Task", description = "Task API")
public class TaskController {

    private final TaskService service;
    private final TaskMapper mapper;

    private String getUserName(Principal principal) {
        Authentication authentication = (Authentication) principal;
        User user = (User) authentication.getPrincipal();
        return user.getUsername();
    }

    @GetMapping("")
    @Operation(summary = "Получение списка задач",
            parameters = {
                    @Parameter(in = ParameterIn.QUERY, name = "active", description = "выборка только активных задач = true"),
                    @Parameter(in = ParameterIn.QUERY, name = "q", description = "выборка задач, содержащих указанную подстроку")
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Успешное получение списка соответствующих задач",
                            content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Task.class)))),
                    @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))),
                    @ApiResponse(responseCode = "404", description = "Задача не найдена",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))),
                    @ApiResponse(responseCode = "500", description = "Внутренняя ошибка сервера",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    List<TaskDto> getTasks(
            @RequestParam(value = "active", required = false, defaultValue = "false") boolean active,
            @RequestParam(value = "q", required = false) String q,
            Principal principal
    ) {
        log.debug("tasks.getAll {}{}{}", active ? "active " : "", q == null ? "" : "with text contains ", q);
        return mapper.toDto(service.find(active, q, getUserName(principal)));

    }

    @PostMapping("")
    @Operation(summary = "Создание задачи",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Успешное создание задачи",
                            content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Task.class)))),
                    @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))),
                    @ApiResponse(responseCode = "500", description = "Внутренняя ошибка сервера",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    TaskDto newTask(@Valid @RequestBody(description = "Задача", required = true,
            content = @Content(schema = @Schema(implementation = TextRq.class)))
                    @org.springframework.web.bind.annotation.RequestBody TextRq textRq,
                    Principal principal
    ) {
        log.debug("task.create {}", textRq.getText());
        return mapper.toDto(service.create(textRq.getText(), getUserName(principal)));
    }

    @Validated
    @GetMapping("/{id}")
    @Operation(summary = "Получение задачи по id",
            parameters = {
                    @Parameter(in = ParameterIn.PATH, name = "id", description = "Идентификатор задачи")
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Успешное получение задачи",
                            content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Task.class)))),
                    @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))),
                    @ApiResponse(responseCode = "404", description = "Задача не найдена",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))),
                    @ApiResponse(responseCode = "500", description = "Внутренняя ошибка сервера",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    TaskDto getTask(@PathVariable @NonNull long id,
                    Principal principal
    ) {
        log.debug("task.findById {}", id);
        return mapper.toDto(service.getById(id, getUserName(principal)));
    }

    @PatchMapping("/{id}/edit")
    @Operation(summary = "Изменение текста задачи",
            parameters = {
                    @Parameter(in = ParameterIn.PATH, name = "id", description = "Идентификатор задачи")
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Успешное изменение текста задачи",
                            content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Task.class)))),
                    @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))),
                    @ApiResponse(responseCode = "404", description = "Задача не найдена",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))),
                    @ApiResponse(responseCode = "500", description = "Внутренняя ошибка сервера",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    TaskDto updateTask(@PathVariable @NonNull long id, @Valid @RequestBody(description = "Задача", required = true,
            content = @Content(schema = @Schema(implementation = TextRq.class)))
    @org.springframework.web.bind.annotation.RequestBody TextRq textRq,
                       Principal principal
    ) {
        log.debug("task.update {} with new text {}", id, textRq.getText());
        return mapper.toDto(service.update(id, textRq.getText(), getUserName(principal)));
    }

    @PatchMapping("/{id}/done")
    @Operation(summary = "Изменение статуса задачи активная/выполненная",
            parameters = {
                    @Parameter(in = ParameterIn.PATH, name = "id", description = "Идентификатор задачи")
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Успешное изменение статуса задачи",
                            content = @Content(mediaType = "application/json", array = @ArraySchema(schema = @Schema(implementation = Task.class)))),
                    @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))),
                    @ApiResponse(responseCode = "404", description = "Задача не найдена",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))),
                    @ApiResponse(responseCode = "500", description = "Внутренняя ошибка сервера",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    TaskDto toggleTask(@PathVariable @NonNull long id, @Valid @RequestBody(description = "Состояние задачи", required = true,
            content = @Content(schema = @Schema(implementation = DoneRq.class)))
    @org.springframework.web.bind.annotation.RequestBody DoneRq doneRq,
                       Principal principal
    ) {
        log.debug("task.toggle {}", id);
        return mapper.toDto(service.toggle(id, doneRq.isDone(), getUserName(principal)));
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удаление задачи",
            parameters = {
                    @Parameter(in = ParameterIn.PATH, name = "id", description = "Идентификатор задачи")
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "Успешное удаление задачи"),
                    @ApiResponse(responseCode = "400", description = "Некорректный запрос",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))),
                    @ApiResponse(responseCode = "404", description = "Задача не найдена",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class))),
                    @ApiResponse(responseCode = "500", description = "Внутренняя ошибка сервера",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ErrorResponse.class)))
            }
    )
    void deleteTask(@PathVariable @NonNull long id,
                    Principal principal
    ) {
        log.debug("task.delete {}", id);
        service.delete(id, getUserName(principal));
    }

}

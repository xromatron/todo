package com.xromatron.todo.service;

import com.xromatron.todo.exception.TaskNotFoundException;
import com.xromatron.todo.model.Account;
import com.xromatron.todo.model.Status;
import com.xromatron.todo.model.Task;
import com.xromatron.todo.repository.AccountRepository;
import com.xromatron.todo.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class TaskService {

    private final TaskRepository taskRepository;
    private final AccountRepository accountRepository;

    public List<Task> find(boolean isActive, String query, String login) {

        if (isActive) {
            if (StringUtils.isBlank(query)) {
                return taskRepository.findByStatusAndAccount_Login(Status.ACTIVE, login);
            } else {
                return taskRepository.findByStatusAndTextContainsAndAccount_Login(Status.ACTIVE, query, login);
            }
        } else {
            if (StringUtils.isBlank(query)) {
                return taskRepository.findByStatusNotAndAccount_Login(Status.DELETED, login);
            } else {
                return taskRepository.findByStatusNotAndTextContainsAndAccount_Login(Status.DELETED, query, login);
            }
        }
    }

    public Task create(String text, String login) {
        Account account = accountRepository.getByLogin(login);
        return taskRepository.save(new Task(text, account));
    }

    public Task getById(long id, String login) {
        return taskRepository.findByIdAndAccount_Login(id, login).orElseThrow(() -> new TaskNotFoundException(id));
    }

    public Task update(long id, String text, String login) {
        return taskRepository.findByIdAndStatusNotAndAccount_Login(id, Status.DELETED, login)
                .map(task -> {
                    task.setText(text);
                    return taskRepository.save(task);
                })
                .orElseThrow(() -> new TaskNotFoundException(id));
    }

    public Task toggle(long id, boolean isDone, String login) {
        return taskRepository.findByIdAndStatusNotAndAccount_Login(id, Status.DELETED, login)
                .map(task -> {
                    if (isDone) {
                        task.setStatus(Status.COMPLETED);
                    } else {
                        task.setStatus(Status.ACTIVE);
                    }
                    return taskRepository.save(task);
                })
                .orElseThrow(() -> new TaskNotFoundException(id));
    }

    public void delete(long id, String login) {
        taskRepository.findByIdAndStatusNotAndAccount_Login(id, Status.DELETED, login)
                .map(task -> {
                    task.setStatus(Status.DELETED);
                    return taskRepository.save(task);
                })
                .orElseThrow(() -> new TaskNotFoundException(id));
    }

}

package com.xromatron.todo.service;

import com.google.common.collect.ImmutableList;
import com.xromatron.todo.model.Account;
import com.xromatron.todo.repository.AccountRepository;
import com.xromatron.todo.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    public Optional<Account> getByLogin(@NotBlank String login) {
        return Optional.ofNullable(accountRepository.getByLogin(login));
    }

    public Account saveUser(String login, String password) {
        Account account = new Account();
        account.setLogin(login);
        account.setPassword(passwordEncoder.encode(password));
        account.setRoles(ImmutableList.of(roleRepository.getByRole("USER")));
        log.info("Creating account: {}", account);
        return accountRepository.save(account);
    }
}

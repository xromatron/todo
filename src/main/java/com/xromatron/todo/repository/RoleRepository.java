package com.xromatron.todo.repository;

import com.xromatron.todo.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role getByRole(String role);
}

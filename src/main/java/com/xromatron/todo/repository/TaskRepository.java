package com.xromatron.todo.repository;

import com.xromatron.todo.model.Status;
import com.xromatron.todo.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long> {

    List<Task> findByStatusAndAccount_Login(Status status, String login);
    List<Task> findByStatusAndTextContainsAndAccount_Login(Status status, String substring, String login);
    List<Task> findByStatusNotAndAccount_Login(Status status, String login);
    List<Task> findByStatusNotAndTextContainsAndAccount_Login(Status status, String substring, String login);
    Optional<Task> findByIdAndAccount_Login(Long id, String login);
    Optional<Task> findByIdAndStatusNotAndAccount_Login(Long id, Status status, String login);
}

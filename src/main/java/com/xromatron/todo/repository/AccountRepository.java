package com.xromatron.todo.repository;

import com.xromatron.todo.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Integer> {
    Account getByLogin(String login);
}

package com.xromatron.todo.wrapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;


@RestControllerAdvice
public class GlobalResponseWrapper implements ResponseBodyAdvice<Object> {

    public static final Set<String> EXCLUSIONS_CONTAINS = newHashSet(
            "/actuator", "/api-docs", "/swagger-ui",
            "/account");

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        String className = returnType.getContainingClass().getSimpleName();
        return !className.contains("Exception");
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                  ServerHttpRequest request, ServerHttpResponse response) {
        String uriPath = request.getURI().getPath();
        boolean containsIgnored = EXCLUSIONS_CONTAINS.stream().anyMatch(uriPath::contains);
        return containsIgnored ? body : wrapResponse(body);
    }

    private ResponseWrapper wrapResponse(Object body) {
        return new ResponseWrapper(body);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class ResponseWrapper {
        private boolean success = true;
        private Object data;

        public ResponseWrapper(Object data) {
            this.data = data;
        }
    }
}

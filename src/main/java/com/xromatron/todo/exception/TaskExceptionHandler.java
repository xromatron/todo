package com.xromatron.todo.exception;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@ControllerAdvice
public class TaskExceptionHandler {

    @ResponseBody
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(NOT_FOUND)
    ErrorResponse noHandler(NoHandlerFoundException ex) {
        log.error("Resource not found", ex);
        return new ErrorResponse("Указан некорректный адрес запроса");
    }

    @ResponseBody
    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    ErrorResponse defaultHandler(RuntimeException ex) {
        log.error("Runtime exception", ex);
        return new ErrorResponse(ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(TaskNotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    ErrorResponse taskNotFoundHandler(TaskNotFoundException ex) {
        log.error("Task not found", ex);
        return new ErrorResponse(ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    ErrorResponse notValidArgumentHandler(MethodArgumentNotValidException ex) {
        log.error("Invalid method argument", ex);
        return new ErrorResponse("Переданы некорректные параметры запроса");
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(BAD_REQUEST)
    ErrorResponse typeMismatchArgumentHandler(MethodArgumentTypeMismatchException ex) {
        log.error("Invalid method argument type", ex);
        return new ErrorResponse("Переданы параметры запроса некорректного типа");
    }

    @Data
    @NoArgsConstructor
    public static class ErrorResponse {
        private boolean success;
        private String message;

        public ErrorResponse(String message) {
            this.message = message;
            this.success = false;
        }
    }

}

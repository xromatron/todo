package com.xromatron.todo.exception;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException(long id) {
        super(String.format("Задача %s не найдена", id));
    }
}

package com.xromatron.todo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "task")
@Getter
@Setter
@NoArgsConstructor
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ToString.Include
    @NotBlank
    private String text;
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private Status status;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private Account account;

    public Task(String text) {
        this.text = text;
        this.status = Status.ACTIVE;
    }

    public Task(String text, Account account) {
        this.text = text;
        this.status = Status.ACTIVE;
        this.account = account;
    }

    public Task(Long id, String text) {
        this.id = id;
        this.text = text;
        this.status = Status.ACTIVE;
    }
}

package com.xromatron.todo.model;

public enum Status {
    ACTIVE,
    COMPLETED,
    DELETED
}

package com.xromatron.todo.mapper;

import com.xromatron.todo.dto.TaskDto;
import com.xromatron.todo.model.Task;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TaskMapperTest {

    @Test
    void toDto() {
        Task task = new Task("text");
        task.setId(1L);

        TaskDto taskDto = TaskMapper.INSTANCE.toDto(task);

        assertThat(taskDto).isNotNull();
        assertThat(taskDto.getId()).isEqualTo(1L);
        assertThat(taskDto.getText()).isEqualTo("text");
        assertThat(taskDto.getStatus()).isEqualTo("ACTIVE");
    }
}
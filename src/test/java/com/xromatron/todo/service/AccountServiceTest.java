package com.xromatron.todo.service;

import com.xromatron.todo.model.Account;
import com.xromatron.todo.model.Role;
import com.xromatron.todo.repository.AccountRepository;
import com.xromatron.todo.repository.RoleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    @InjectMocks
    AccountService service;
    @Captor
    ArgumentCaptor<Account> accountCaptor;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private RoleRepository roleRepository;
    private final String LOGIN = "user";
    @Spy
    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Test
    void getByLoginTest() {
        when(accountRepository.getByLogin(LOGIN)).thenReturn(null);

        service.getByLogin(LOGIN);

        verify(accountRepository).getByLogin(LOGIN);
    }

    @Test
    void saveUserTest() {
        final String password = "password";
        final String role = "USER";
        when(roleRepository.getByRole(role)).thenReturn(new Role(role));
        when(accountRepository.save(any(Account.class))).thenReturn(null);

        service.saveUser(LOGIN, password);

        verify(accountRepository).save(accountCaptor.capture());
        verify(passwordEncoder).encode(password);
        Account account = accountCaptor.getValue();
        assertAll(() -> {
            assertEquals(LOGIN, account.getLogin());
            assertThat(account.getPassword(), not(containsString(password)));
            assertEquals(role, account.getRoles().get(0).getRole());
        });
    }
}
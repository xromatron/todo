package com.xromatron.todo.service;

import com.xromatron.todo.exception.TaskNotFoundException;
import com.xromatron.todo.model.Status;
import com.xromatron.todo.model.Task;
import com.xromatron.todo.repository.AccountRepository;
import com.xromatron.todo.repository.TaskRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TaskServiceTest {

    private final String LOGIN = "user";
    private final String TEXT_SAMPLE_ONE = "text1";
    private final String TEXT_SAMPLE_TWO = "text2";
    private final Long ID = 1L;
    private final String ID_NOT_FOUND = "Задача " + ID + " не найдена";
    @InjectMocks
    TaskService service;
    @Captor
    ArgumentCaptor<Task> taskCaptor;
    @Mock
    private TaskRepository taskRepository;
    @Mock
    private AccountRepository accountRepository;

    @Test
    void find_shouldReturnList_whenIsActiveTrueAndQueryIsEmpty() {
        when(taskRepository.findByStatusAndAccount_Login(Status.ACTIVE, LOGIN))
                .thenReturn(Collections.emptyList());

        service.find(true, "", LOGIN);

        verify(taskRepository, times(1))
                .findByStatusAndAccount_Login(Status.ACTIVE, LOGIN);
        verifyNoMoreInteractions(taskRepository);
    }

    @Test
    void find_shouldReturnList_whenIsActiveTrueAndQueryIsNotEmpty() {
        when(taskRepository.findByStatusAndTextContainsAndAccount_Login(any(Status.class), anyString(), anyString()))
                .thenReturn(Collections.emptyList());

        service.find(true, TEXT_SAMPLE_ONE, LOGIN);

        verify(taskRepository, times(1))
                .findByStatusAndTextContainsAndAccount_Login(Status.ACTIVE, TEXT_SAMPLE_ONE, LOGIN);
        verifyNoMoreInteractions(taskRepository);
    }

    @Test
    void find_shouldReturnList_whenIsActiveFalseAndQueryIsEmpty() {
        when(taskRepository.findByStatusNotAndAccount_Login(any(Status.class), anyString()))
                .thenReturn(Collections.emptyList());

        service.find(false, "", LOGIN);

        verify(taskRepository, times(1))
                .findByStatusNotAndAccount_Login(Status.DELETED, LOGIN);
        verifyNoMoreInteractions(taskRepository);
    }

    @Test
    void find_shouldReturnList_whenIsActiveFalseAndQueryIsNotEmpty() {
        when(taskRepository.findByStatusNotAndTextContainsAndAccount_Login(any(Status.class), anyString(), anyString()))
                .thenReturn(Collections.emptyList());

        service.find(false, TEXT_SAMPLE_ONE, LOGIN);

        verify(taskRepository, times(1))
                .findByStatusNotAndTextContainsAndAccount_Login(Status.DELETED, TEXT_SAMPLE_ONE, LOGIN);
        verifyNoMoreInteractions(taskRepository);
    }

    @Test
    void createTest() {
        when(accountRepository.getByLogin(LOGIN))
                .thenReturn(null);

        service.create(TEXT_SAMPLE_ONE, LOGIN);

        verify(accountRepository, times(1))
                .getByLogin(LOGIN);
        verify(taskRepository, times(1))
                .save(taskCaptor.capture());
        Task task = taskCaptor.getValue();
        assertEquals(TEXT_SAMPLE_ONE, task.getText());
        assertEquals(null, task.getAccount());
    }

    @Test
    void getById_shouldReturnTask_whenIdExists() {
        when(taskRepository.findByIdAndAccount_Login(anyLong(), anyString()))
                .thenReturn(Optional.of(new Task(ID, TEXT_SAMPLE_ONE)));

        Task task = service.getById(ID, LOGIN);

        verify(taskRepository, times(1))
                .findByIdAndAccount_Login(ID, LOGIN);
        assertEquals(ID, task.getId());
        assertEquals(TEXT_SAMPLE_ONE, task.getText());
        assertEquals(Status.ACTIVE, task.getStatus());
    }

    @Test
    void getById_shouldThrowException_whenIdDoesNotExist() {
        when(taskRepository.findByIdAndAccount_Login(anyLong(), anyString()))
                .thenReturn(Optional.ofNullable(null));

        TaskNotFoundException exception = assertThrows(TaskNotFoundException.class,
                () -> service.getById(ID, LOGIN));

        verify(taskRepository, times(1))
                .findByIdAndAccount_Login(ID, LOGIN);
        assertEquals(ID_NOT_FOUND, exception.getMessage());
    }

    @Test
    void update_shouldReturnTask_whenIdExists() {
        Task task = new Task(ID, TEXT_SAMPLE_ONE);
        when(taskRepository.findByIdAndStatusNotAndAccount_Login(anyLong(), any(Status.class), anyString()))
                .thenReturn(Optional.of(task));
        when(taskRepository.save(any(Task.class)))
                .thenReturn(task);

        service.update(ID, TEXT_SAMPLE_TWO, LOGIN);

        verify(taskRepository, times(1))
                .findByIdAndStatusNotAndAccount_Login(ID, Status.DELETED, LOGIN);
        verify(taskRepository, times(1))
                .save(taskCaptor.capture());
        assertEquals(ID, task.getId());
        assertEquals(TEXT_SAMPLE_TWO, task.getText());
        assertEquals(Status.ACTIVE, task.getStatus());
    }

    @Test
    void update_shouldThrowException_whenIdDoesNotExist() {
        when(taskRepository.findByIdAndStatusNotAndAccount_Login(anyLong(), any(Status.class), anyString()))
                .thenReturn(Optional.ofNullable(null));

        TaskNotFoundException exception = assertThrows(TaskNotFoundException.class,
                () -> service.update(ID, TEXT_SAMPLE_TWO, LOGIN));

        verify(taskRepository, times(1))
                .findByIdAndStatusNotAndAccount_Login(ID, Status.DELETED, LOGIN);
        verifyNoMoreInteractions(taskRepository);
        assertEquals(ID_NOT_FOUND, exception.getMessage());
    }

    @Test
    void toggle_shouldReturnTask_whenIdExistsAndIsDoneTrue() {
        Task task = new Task(ID, TEXT_SAMPLE_ONE);
        when(taskRepository.findByIdAndStatusNotAndAccount_Login(anyLong(), any(Status.class), anyString()))
                .thenReturn(Optional.of(task));
        when(taskRepository.save(any(Task.class)))
                .thenReturn(task);

        service.toggle(ID, true, LOGIN);

        verify(taskRepository, times(1))
                .findByIdAndStatusNotAndAccount_Login(ID, Status.DELETED, LOGIN);
        verify(taskRepository, times(1))
                .save(taskCaptor.capture());
        task = taskCaptor.getValue();
        assertEquals(ID, task.getId());
        assertEquals(TEXT_SAMPLE_ONE, task.getText());
        assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    void toggle_shouldReturnTask_whenIdExistsAndIsDoneFalse() {
        Task task = new Task(ID, TEXT_SAMPLE_ONE);
        when(taskRepository.findByIdAndStatusNotAndAccount_Login(anyLong(), any(Status.class), anyString()))
                .thenReturn(Optional.of(task));
        when(taskRepository.save(any(Task.class)))
                .thenReturn(task);

        service.toggle(ID, false, LOGIN);

        verify(taskRepository, times(1))
                .findByIdAndStatusNotAndAccount_Login(ID, Status.DELETED, LOGIN);
        verify(taskRepository, times(1))
                .save(taskCaptor.capture());
        task = taskCaptor.getValue();
        assertEquals(ID, task.getId());
        assertEquals(TEXT_SAMPLE_ONE, task.getText());
        assertEquals(Status.ACTIVE, task.getStatus());
    }

    @Test
    void toggle_shouldThrowException_whenIdDoesNotExist() {
        Task task = new Task(ID, TEXT_SAMPLE_ONE);
        when(taskRepository.findByIdAndStatusNotAndAccount_Login(anyLong(), any(Status.class), anyString()))
                .thenReturn(Optional.ofNullable(null));

        TaskNotFoundException exception = assertThrows(TaskNotFoundException.class,
                () -> service.toggle(ID, true, LOGIN));

        verify(taskRepository, times(1))
                .findByIdAndStatusNotAndAccount_Login(ID, Status.DELETED, LOGIN);
        verifyNoMoreInteractions(taskRepository);
        assertEquals(ID_NOT_FOUND, exception.getMessage());
    }

    @Test
    void delete_shouldReturnTask_whenIdExists() {
        Task task = new Task(ID, TEXT_SAMPLE_ONE);
        when(taskRepository.findByIdAndStatusNotAndAccount_Login(anyLong(), any(Status.class), anyString()))
                .thenReturn(Optional.of(task));
        when(taskRepository.save(any(Task.class)))
                .thenReturn(task);

        service.delete(ID, LOGIN);

        verify(taskRepository, times(1))
                .findByIdAndStatusNotAndAccount_Login(ID, Status.DELETED, LOGIN);
        verify(taskRepository, times(1))
                .save(taskCaptor.capture());
        task = taskCaptor.getValue();
        assertEquals(ID, task.getId());
        assertEquals(TEXT_SAMPLE_ONE, task.getText());
        assertEquals(Status.DELETED, task.getStatus());
    }

    @Test
    void delete_shouldThrowException_whenIdDoesNotExist() {
        when(taskRepository.findByIdAndStatusNotAndAccount_Login(anyLong(), any(Status.class), anyString()))
                .thenReturn(Optional.ofNullable(null));

        TaskNotFoundException exception = assertThrows(TaskNotFoundException.class,
                () -> service.delete(ID, LOGIN));

        verify(taskRepository, times(1))
                .findByIdAndStatusNotAndAccount_Login(ID, Status.DELETED, LOGIN);
        verifyNoMoreInteractions(taskRepository);
        assertEquals(ID_NOT_FOUND, exception.getMessage());
    }
}
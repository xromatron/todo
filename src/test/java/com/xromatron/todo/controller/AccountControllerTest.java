package com.xromatron.todo.controller;

import com.google.common.collect.ImmutableList;
import com.xromatron.todo.model.Account;
import com.xromatron.todo.model.Role;
import com.xromatron.todo.service.AccountService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AccountController.class)
class AccountControllerTest {

    private final String USER_NAME = "user1";
    private final String PASSWORD = "password1";
    private final String ENCODED_PASSWORD = "encoded-" + PASSWORD;

    @MockBean
    AccountService service;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(username = "admin", roles = "ADMIN")
    void register_shouldReturn200_whenAdmin() throws Exception {
        Account account = new Account();
        account.setLogin(USER_NAME);
        account.setPassword(ENCODED_PASSWORD);
        account.setRoles(ImmutableList.of(new Role("USER")));
        String expected = "Account of user '" + USER_NAME + "' created! Encoded password: '" + ENCODED_PASSWORD + "'";

        Mockito.when(service.saveUser(USER_NAME, PASSWORD)).thenReturn(account);
        mockMvc.perform(
                        post("/account/register?login=" + USER_NAME + "&password=" + PASSWORD)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(expected)))
        ;
    }

    @Test
    @WithMockUser
    void register_shouldReturn403_whenUser() throws Exception {
        mockMvc.perform(
                        post("/account/register?login=" + USER_NAME + "&password=" + PASSWORD)
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isForbidden())
        ;
    }

}
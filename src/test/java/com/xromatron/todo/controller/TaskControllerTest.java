package com.xromatron.todo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xromatron.todo.dto.DoneRq;
import com.xromatron.todo.dto.TextRq;
import com.xromatron.todo.mapper.TaskMapper;
import com.xromatron.todo.mapper.TaskMapperImpl;
import com.xromatron.todo.model.Status;
import com.xromatron.todo.model.Task;
import com.xromatron.todo.service.TaskService;
import com.xromatron.todo.wrapper.GlobalResponseWrapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TaskController.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ContextConfiguration(classes = {GlobalResponseWrapper.class, TaskController.class, TaskMapper.class, TaskMapperImpl.class})
class TaskControllerTest {

    @MockBean
    TaskService service;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    MockMvc mockMvc;
    private List<Task> tasks;

    @BeforeAll
    void beforeAll() {
        tasks = LongStream.rangeClosed(1L, 3L)
                .mapToObj(i -> {
                    Task task = new Task("text" + i);
                    task.setId(i);
                    task.setStatus(Status.values()[(int) i - 1]);
                    return task;
                })
                .collect(Collectors.toList());
    }

    @Test
    @WithMockUser
    void getTasks() throws Exception {
        when(service.find(anyBoolean(), anyString(), anyString())).thenReturn(tasks);

        mockMvc.perform(
                        get("/tasks?active=true&q=text")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value("true"))
                .andExpect(jsonPath("$.data").isNotEmpty())
                .andExpect(jsonPath("$.data").isArray())
                .andExpect(jsonPath("$.data", hasSize(3)))
                .andExpect(jsonPath("$.data[0].id").value(1L))
                .andExpect(jsonPath("$.data[0].text", containsString("text")))
                .andExpect(jsonPath("$.data[0].status").value("ACTIVE"))
        ;
    }

    @Test
    @WithMockUser
    void newTask() throws Exception {
        when(service.create(anyString(), anyString())).thenReturn(tasks.get(0));

        mockMvc.perform(
                        post("/tasks")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(new TextRq("text")))
                                .with(csrf())
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value("true"))
                .andExpect(jsonPath("$.data").isNotEmpty())
                .andExpect(jsonPath("$.data.id").value(1L))
                .andExpect(jsonPath("$.data.text", containsString("text")))
                .andExpect(jsonPath("$.data.status").value("ACTIVE"))
        ;
    }

    @Test
    @WithMockUser
    void getTask() throws Exception {
        when(service.getById(anyLong(), anyString())).thenReturn(tasks.get(0));

        mockMvc.perform(
                        get("/tasks/1")
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value("true"))
                .andExpect(jsonPath("$.data").isNotEmpty())
                .andExpect(jsonPath("$.data.id").value(1L))
                .andExpect(jsonPath("$.data.text", containsString("text")))
                .andExpect(jsonPath("$.data.status").value("ACTIVE"))
        ;
    }

    @Test
    @WithMockUser
    void updateTask() throws Exception {
        when(service.update(anyLong(), anyString(), anyString())).thenReturn(tasks.get(0));

        mockMvc.perform(
                        patch("/tasks/1/edit")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(new TextRq("text")))
                                .with(csrf())
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value("true"))
                .andExpect(jsonPath("$.data").isNotEmpty())
                .andExpect(jsonPath("$.data.id").value(1L))
                .andExpect(jsonPath("$.data.text", containsString("text")))
                .andExpect(jsonPath("$.data.status").value("ACTIVE"))
        ;
    }

    @Test
    @WithMockUser
    void toggleTask() throws Exception {
        when(service.toggle(anyLong(), anyBoolean(), anyString())).thenReturn(tasks.get(1));

        mockMvc.perform(
                        patch("/tasks/1/done")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(new DoneRq()))
                                .with(csrf())
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value("true"))
                .andExpect(jsonPath("$.data").isNotEmpty())
                .andExpect(jsonPath("$.data.id").value(2L))
                .andExpect(jsonPath("$.data.text", containsString("text")))
                .andExpect(jsonPath("$.data.status").value("COMPLETED"))
        ;
    }

    @Test
    @WithMockUser
    void deleteTask() throws Exception {
        doNothing().when(service).delete(anyLong(), anyString());

        mockMvc.perform(
                        delete("/tasks/1")
                                .with(csrf())
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value("true"))
                .andExpect(jsonPath("$.data").isEmpty())
        ;
    }

}
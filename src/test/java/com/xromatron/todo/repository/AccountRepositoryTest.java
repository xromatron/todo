package com.xromatron.todo.repository;

import com.google.common.collect.ImmutableList;
import com.xromatron.todo.model.Account;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class AccountRepositoryTest {

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @Autowired
    TestEntityManager entityManager;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    RoleRepository roleRepository;

    @Test
    void getByLogin_shouldReturnAdminAccount_whenLiquibaseEnabled() {
        Account account = accountRepository.getByLogin("admin");
        assertThat(account).isNotNull();
        assertThat(account.getLogin()).isEqualTo("admin");
    }

    @Test
    void getByLogin_shouldReturnUserAccount_whenLoginIsUser() {
        Account newAccount = new Account();
        newAccount.setLogin("testLogin1");
        newAccount.setPassword(passwordEncoder.encode("testPassword2"));
        newAccount.setRoles(ImmutableList.of(roleRepository.getByRole("USER")));
        entityManager.persist(newAccount);
        entityManager.flush();
        Account account = accountRepository.getByLogin("testLogin1");
        assertThat(account).isNotNull();
        assertThat(account.getLogin()).isEqualTo("testLogin1");
    }

    @Test
    void save_shouldReturnNewAccount_whenSaveNewAccount() {
        Account newAccount = new Account();
        newAccount.setLogin("testLogin2");
        newAccount.setPassword(passwordEncoder.encode("testPassword2"));
        newAccount.setRoles(ImmutableList.of(roleRepository.getByRole("USER")));
        Account expectedAccount = accountRepository.save(newAccount);
        assertThat(expectedAccount).isNotNull();
        assertThat(expectedAccount.getLogin()).isEqualTo("testLogin2");
    }
}
package com.xromatron.todo.repository;

import com.google.common.collect.ImmutableList;
import com.xromatron.todo.model.Account;
import com.xromatron.todo.model.Task;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;


class TaskRepositoryTest extends DatabaseIT {

    private final String LOGIN = "testLogin1";
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    TaskRepository taskRepository;

    @Test
    void testGetByLogin_shouldReturnAdmin_whenAdminExist() {
        Account account = accountRepository.getByLogin("admin");
        assertThat(account.getLogin()).isEqualTo("admin");
    }

    @Test
    void testGetByLogin_shouldReturnUser_whenCreated() {
        createUser();
        Account account = accountRepository.getByLogin(LOGIN);
        assertNotNull(account);
    }

    @Test
    void testGetByLogin_shouldReturnNull_whenNotCreated() {
        Account account = accountRepository.getByLogin(LOGIN);
        assertNull(account);
    }

    @Test
    void testGetById_shouldReturnTask_whenSaved() {
        Account account = createUser();
        Task newTask = new Task("testTask1", account);
        Task savedTask = taskRepository.save(newTask);
        Task task = taskRepository.getById(savedTask.getId());
        assertNotNull(task);
    }

    private Account createUser() {
        Account newAccount = new Account();
        newAccount.setLogin(LOGIN);
        newAccount.setPassword(passwordEncoder.encode("testPassword2"));
        newAccount.setRoles(ImmutableList.of(roleRepository.getByRole("USER")));
        return accountRepository.save(newAccount);
    }
}
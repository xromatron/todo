package com.xromatron.todo.repository;

import com.xromatron.todo.model.Role;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class RoleRepositoryTest {

    @Autowired
    RoleRepository roleRepository;

    @Test
    void getByRole_shouldReturnUserRole_whenLiquibaseEnabled() {
        Role role = roleRepository.getByRole("USER");
        assertThat(role).isNotNull();
        assertThat(role.getRole()).isEqualTo("USER");
    }
}
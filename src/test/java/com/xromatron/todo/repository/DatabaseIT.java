package com.xromatron.todo.repository;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@Testcontainers
public abstract class DatabaseIT {

    @Container
    public static TestPostgresContainer postgres = TestPostgresContainer
            .getInstance();

    @Test
    void testContainerIsRunning() {
        assertTrue(postgres.isRunning());
    }

}
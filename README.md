# ToDo App v1.30

## Описание API:
http://localhost:8080/swagger-ui

## Тестирование API:  
  - via Swagger
  - via src/test/resources/task-test.http

## Параметры запуска
для запуска приложение с логгированием на уровне DEBUG необходимо установить VM options: `-Dlog4j2.level=DEBUG`
```
mvm clean package
cd target
java "-Dsun.stdout.encoding=UTF-8" "-Dlog4j2.level=DEBUG" -jar todo-1.30.jar
```
по умолчанию установлен уровень INFO  
логи записываются в файл `todo.log` в папке запуска

## Уровень покрытия
установлен в `pom.xml`
```xml
<properties>
    <jacoco.coverage>0.20</jacoco.coverage>
</properties>
```
Чтобы проект не собрался при уровне покрытия ниже установленного,
необходимо запустить:

`mvn clean verify`

После этого сформированный отчет можно посмотреть:

`target/site/jacoco/index.html`